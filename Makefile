NAME = ft_ls 

SRC = src/ft_ls.c \
		src/ft_printf.c 

INCLUDES =  -Iinclude -Ilibft

LIBOBJ = $(SRC:.c=.o)

CFLAGS = -Wall -Werror -Wextra

C_CYAN = \033[36m

all: $(NAME)

$(NAME): $(LIBOBJ) $(D_SDL)
		make -C ./libft
		gcc $(CFLAGS) $(LIBOBJ) $(INCLUDES) -L./libft -lft -lm -o $(NAME)
%.o:%.c include/ft_ls.h
		gcc -MD -g -O0 -c $<  -o $@

clean:
	make -C ./libft clean
	-rm -rf $(LIBOBJ)
	-rm -f src/*.d

fclean: clean
	make -C ./libft fclean
	-rm -f $(NAME)
	-rm -f src/*.d

re: fclean all
