#ifndef FT_LS_FT_LS_H
# define FT_LS_FT_LS_H

#include <unistd.h>
#include <stdlib.h>
#include "../libft/libft.h"
#include <stdio.h>

void		ft_print_string(char* st);
void 	    ft_print_int(int i);
void		ft_putchar(char ch);

#endif
